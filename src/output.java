//Chris Hinson
//Chapter 4 Program 3
//This class handles the programs output, and reatain console output from program 2
//while adding and implementing the ability to write tot a file

import java.io.IOException;
import java.io.*;

public class output {

    public static void printGraph(double speed, double hours)
    {
        System.out.println("Hour    Distance Traveled");
        System.out.println("-------------------------");
        for (int i=1; i<=hours; i++)
        {
        System.out.println(i + "       " + speed*i);
        }
    }

    public static void fileOutput(double speed, double hours) throws IOException
    {
        String filepath = "speedChart.txt";
        PrintWriter outputFile = new PrintWriter(filepath);
        outputFile.println("Hour    Distance Traveled");
        outputFile.println("-------------------------");
        for (int i=1; i<=hours; i++)
        {
            outputFile.println(i + "       " + speed*i);
        }
        outputFile.close();
        System.out.println("Your results have been output to " + filepath + " in the same file as the source code for this program");
    }
}
