//Chris Hinson
//Chapter 4 Program 3
//This class handles the user's input

import java.util.Scanner;

public class userInput
{

    Scanner keyboard = new Scanner(System.in);

    public double getSpeed()
    {

        double speed = 0;
        Boolean validated =Boolean.FALSE;


        System.out.println("Please enter the speed of the vehicle in mph.");

        do {
            try {
                speed = keyboard.nextDouble();

                    if (speed > 0) {
                        validated = Boolean.TRUE;
                    }
                    else
                    {
                        System.out.println("Speed cannot be negative");
                    }
            } catch (java.util.InputMismatchException e) {
                System.out.println("That's not a number");
                keyboard.next();
            }
        }
        while(!validated);



        System.out.println("Your speed entered is " + speed);

        return speed;
    }

    public double getHours()
    {
        double hours = 0;
        Boolean validated = Boolean.FALSE;


        System.out.println("Please enter the time traveled (in hours)");

        do {

            try {
                hours = keyboard.nextDouble();

                if(hours > 1)
                {
                    validated = Boolean.TRUE;
                }
                else
                {
                    System.out.println("You cannot have traveled less than 1 hour");
                }
            }
            catch (java.util.InputMismatchException e)
            {
                System.out.println("Hours traveled must be a number");
                keyboard.next();
            }

        }
        while (!validated);

        System.out.println("You have traveled " + hours + " hours");

        return hours;
    }
}
